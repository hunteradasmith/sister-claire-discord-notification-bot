#!/usr/bin/env python3
import traceback
import discord
import asyncio
import yaml
import sys

from datetime import datetime, timezone, timedelta
from rss_checker import get_filtered_updates, get_latest_update

intents = discord.Intents.defaults()
intents.messages = True
intents.dm_messages = True

class Bot(discord.Client):
    def __init__(self, config):
        super().__init__(intents=intents)

        self.rss_url = config['rss_url']
        self.update_frequency = config['update_frequency']
        self.channel_id = config['channel_id']
        self.admin_user_ids = config['admin_user_ids']
        self.operator_user_ids = config['operator_user_ids']
        self.debug_mode = config['debug']

        self.startup = datetime.now(timezone.utc) - timedelta(minutes = 5)
        self.seen_updates = set()

        self.error_state = False # Flag to prevent spamming the admin with repeat error messages
        self.channel = self.admin_users = None
    
    async def on_ready(self):
        asyncio.create_task(self.rss_loop())

        self.channel = self.get_channel(self.channel_id)
        self.admin_users = [self.get_user(_id) for _id in self.admin_user_ids]
        self.operator_users = [self.get_user(_id) for _id in self.operator_user_ids]
        print('Bot ready. Logged in as {}, ID {}'.format(self.user.name, self.user.id))

    async def on_server_join(self, server):
        self.debug('joined server, setting channel')
        self.channel = self.get_channel(self.channel_id)

    async def on_disconnect(self):
        print('disconnected from discord, quitting...')
        quit()

    async def on_error(self, event, *args, **kwargs):
        print('exception', event, args, kwargs)
        print(sys.exc_info())
        print('quitting...')
        quit()

    async def tell_operators(self, message):
        print('telling operators:', message)
        for operator in self.operator_users:
            await operator.send(message)
    
    def debug(self, *args):
        if self.debug_mode: print(*args)

    async def rss_loop(self):
        await self.wait_until_ready()

        self.debug('starting rss_loop loop')
        while not self.is_closed():
            try:
                await self.post_messages()
            except Exception:
                if not self.error_state:
                    self.error_state = True
                    await self.tell_operators('Error checking RSS feed: \n```{}```'.format(traceback.format_exc()))
            else:
                if self.error_state:
                    self.error_state = False
                    await self.tell_operators('Successfully checked RSS feed')

            self.debug('rss_loop sleeping for {} seconds'.format(self.update_frequency))
            await asyncio.sleep(self.update_frequency)

        self.debug('is_closed, ending rss_loop')
        quit()

    async def post_messages(self):
        self.debug('checking for updates')

        updates = await get_filtered_updates(self.rss_url, self.seen_updates, self.startup)
        for update in updates:
            print('posting update: {}'.format(update.message))
            await self.channel.send(update.message)
            self.seen_updates.add(update.id)

    async def on_message(self, message):
        if not self.channel or not self.admin_users: return

        if message.author.id == self.user.id or not (message.channel == self.channel or message.author.id in self.admin_user_ids): return
        elif '!post_latest' in message.content:
            latest = await get_latest_update(self.rss_url)
            print('admin command !post_latest, posting: {}'.format(latest.message))
            await self.channel.send(latest.message)

config = yaml.safe_load(open(sys.argv[1]))
bot = Bot(config)
bot.run(config['token'])
