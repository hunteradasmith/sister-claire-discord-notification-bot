#!/usr/bin/env python3
'''Checks the RSS feed for new comics'''

import asyncio
import aiohttp
import re

from xml.etree import cElementTree as et
from datetime import datetime, timezone, timedelta
from typing import List, Set

class RssException(Exception):
    pass

class RssUpdate:
    def __init__(self, _id, message, _datetime):
        self.id = _id
        self.message = message
        self.datetime = _datetime

    def is_future(self) -> bool:
        return self.datetime > datetime.now(timezone.utc)

    def is_after(self, after: datetime) -> bool:
        return self.datetime > after


### Main Methods ###
async def get_filtered_updates(rss_url: str, filter_ids: Set[str], after: datetime) -> List[RssUpdate]:
    updates = await get_updates(rss_url)
    return filter_updates(updates, filter_ids, after)

async def get_latest_update(rss_url: str) -> RssUpdate:
    updates = await get_updates(rss_url)
    return updates[-1] if updates else None


### Feed Methods ###
async def get_updates(rss_url: str) -> List[RssUpdate]:
    feed_text = await get_feed_text(rss_url)
    return parse_feed_text(feed_text)

async def get_feed_text(rss_url: str) -> str:
    async with aiohttp.ClientSession() as session:
        try:
            async with session.get(rss_url) as resp:
                if not resp.status == 200:
                    raise RssException('RSS feed responded with HTTP status {}, cannot check updates'.format(resp.status))

                return await resp.text()

        except aiohttp.ClientOSError as e:
            raise RssException('system exception') from e

def parse_feed_text(feed_text: str) -> List[RssUpdate]:
    if feed_text == None: return []
    root = et.fromstring(feed_text)

    updates = []
    for item in root.iter('item'):
        _datetime = datetime.strptime(item.find('pubDate').text, '%a, %d %b %Y %H:%M:%S %z')
        url = item.find('link').text
        title = item.find('title').text.replace('RSS - ', '')
        message = 'Update: {} {}'.format(title, url)
        
        late = 'late-page' in url or 'late' in title.lower()
        preview = '(Preview)' in title
        if late or preview:
            html_re = re.compile(r'<.*?>')
            description = html_re.sub('', item.find('description').text.replace('<br />', '\n'))
            message = "Today's update will be late: {}\n```{}```".format(url if preview else '', description)

        updates.append(RssUpdate(url, message, _datetime))

    sorted_updates = sorted(updates, key = lambda u: u.datetime)
    return sorted_updates


### Feed Filter Methods ###
def filter_updates(updates: List[RssUpdate], filter_ids: Set[str], after: datetime) -> List[RssUpdate]:
    return [update for update in updates \
        if not update.id in filter_ids and \
        not update.is_future() and \
        update.is_after(after)]